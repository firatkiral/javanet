
package neuralNet.old;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;


public class dataReader
{
    public static float [][] dataset;
    static int row = 0;
    static List<Integer> shuffleList;

    static void shuffle(int dataCount, Random rnd)
    {
        shuffleList = new ArrayList<Integer>();
        for (int i = 0; i < dataCount; i++)
        {
            shuffleList.add(i);
        }
        Collections.shuffle(shuffleList, rnd);
    }

    public static void read(String filePath, String delimiter, Random rnd, boolean shfl) throws FileNotFoundException, IOException
    {
        filePath = ClassLoader.getSystemClassLoader().getResource(filePath).getFile();
        LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(filePath));
        lineNumberReader.skip(Long.MAX_VALUE);
        int lines = lineNumberReader.getLineNumber();

        if (shfl)
        {
            shuffle(lines, rnd);
            dataset = new float[lines][];
          
            try (Stream<String> liness = Files.lines(Paths.get(filePath), Charset.defaultCharset()))
            {
                liness.forEachOrdered(line -> process(line, delimiter));
            }
            row = 0;
        } else
        {
            dataset = new float[lines][];
            try (Stream<String> liness = Files.lines(Paths.get(filePath), Charset.defaultCharset()))
            {
                liness.forEachOrdered(line -> processNoShfl(line, delimiter));
            }
            row = 0;
        }

    }

    static void process(String s, String delimiter)
    {
        if (s != null)
        {
            String[] strArray = s.split(delimiter);
            float[] floatArray = new float[strArray.length];
            for (int i = 0; i < strArray.length; i++)
            {
                floatArray[i] = Float.parseFloat(strArray[i]);
            }
            dataset[shuffleList.get(row)] = floatArray;
            row++;
        }
    }

    static void processNoShfl(String s, String delimiter)
    {
        if (s != null)
        {
            String[] strArray = s.split(delimiter);
            float[] floatArray = new float[strArray.length];
            for (int i = 0; i < strArray.length; i++)
            {
                floatArray[i] = Float.parseFloat(strArray[i]);
            }
            dataset[row] = floatArray;
            row++;
        }
    }

//    public static List<float[]> dataset = new ArrayList<float[]>();
//    
//    static public void read(String filePath, String delimiter) throws FileNotFoundException, IOException
//    {
//        try (Stream<String> liness = Files.lines(Paths.get(filePath), Charset.defaultCharset())) 
//        {
//            liness.forEachOrdered(line -> process(line, delimiter));
//        }
//    }
//    
//    static void process(String s, String delimiter)
//    {
//        if (s != null)
//        {
//            String[] strArray = s.split(delimiter);
//            float[] doubleArray = new float[strArray.length];
//            for(int i = 0; i < strArray.length; i++) 
//            {
//                doubleArray[i] = Float.parseFloat(strArray[i]);
//            }
//            dataset.add(doubleArray);
//        }
//    }

}
