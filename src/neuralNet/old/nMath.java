
package neuralNet.old;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;


public class nMath
{
    static float[][] times(float[][] A, float B[][])
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = A[m][n] * B[m][n];
                    }
                });
        return temp;
    }

    static float[][] divide(float[][] A, float B[][])
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = A[m][n] / B[m][n];
                    }
                });
        return temp;
    }

    static float[][] plus(float[][] A, float B[][])
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = A[m][n] + B[m][n];
                    }
                });
        return temp;
    }

    static float[][] minus(float[][] A, float B[][])
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = A[m][n] - B[m][n];
                    }
                });
        return temp;
    }

    static float[][] times(float[][] A, float b)
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = A[m][n] * b;
                    }

                });
        return temp;
    }

    static float[][] divide(float[][] A, float b)
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = A[m][n] / b;
                    }

                });
        return temp;
    }

    static float[][] divide(float b, float[][] A)
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = b / A[m][n];
                    }

                });
        return temp;
    }

    static float[][] plus(float[][] A, float b)
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = A[m][n] + b;
                    }

                });
        return temp;
    }

    static float[][] minus(float[][] A, float b)
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = A[m][n] - b;
                    }

                });
        return temp;
    }

    static float[][] minus(float b, float[][] A)
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = b - A[m][n];
                    }

                });
        return temp;
    }

    static void timesEquals(float[][] A, float B[][])
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[m].length; n++)
                    {
                        A[m][n] *= B[m][n];

                    }
                });
    }

    static void divideEquals(float[][] A, float B[][])
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[m].length; n++)
                    {
                        A[m][n] /= B[m][n];

                    }
                });
    }

    static void plusEquals(float[][] A, float B[][])
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[m].length; n++)
                    {
                        A[m][n] += B[m][n];

                    }
                });
    }

    static void minusEquals(float[][] A, float B[][])
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[m].length; n++)
                    {
                        A[m][n] -= B[m][n];

                    }
                });
    }

    static void timesEquals(float[][] A, float b)
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[0].length; n++)
                    {
                        A[m][n] *= b;
                    }
                });
    }

    static void divideEquals(float[][] A, float b)
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[0].length; n++)
                    {
                        A[m][n] /= b;
                    }
                });
    }

    static void divideEquals(float b, float[][] A)
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[0].length; n++)
                    {
                        A[m][n] = b/A[m][n];
                    }
                });
    }

    static void plusEquals(float[][] A, float b)
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[0].length; n++)
                    {
                        A[m][n] += b;
                    }
                });
    }

    static void minusEquals(float[][] A, float b)
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[0].length; n++)
                    {
                        A[m][n] += b;
                    }
                });
    }

    static void minusEquals(float b, float[][] A)
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[0].length; n++)
                    {
                        A[m][n] = b - A[m][n];
                    }
                });
    }

    static float[][] dot(float[][] A, float B[][])
    {
        float[][] temp = new float[A.length][B[0].length];   
//        float[][] bt =  new float[B[0].length][B.length];
//        Tfast(0, B.length, 0, B[0].length, B, bt);
//        
//        IntStream.range(0, A.length)
//                .parallel()
//                .forEach(m ->
//                {
//                    for (int n = 0; n < bt.length; n++)
//                    {
//                        float val = 0; 
//                        for (int k = 0; k < A[0].length; k++)
//                        {
//                            val += bt[n][k] * A[m][k];
//                        }
//                        temp[m][n] = val;
//                    }
//                });
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int k = 0; k < B.length; k++)
                    {
                        for (int n = 0; n < B[0].length; n++)
                        {
                            temp[m][n] += B[k][n] * A[m][k];
                        }
                    }
                });
        return temp;
    }
    
    static float[][] dotT(float[][] A, float B[][])
    {
        float[][] temp = new float[A.length][B.length];   
        
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < B.length; n++)
                    {
                        float val = 0; 
                        for (int k = 0; k < A[0].length; k++)
                        {
                            val += B[n][k] * A[m][k];
                        }
                        temp[m][n] = val;
                    }
                });
        return temp;
    }

    static float[][] sqrt(float[][] A)
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[m].length; n++)
                    {
                        temp[m][n] = (float) Math.sqrt(A[m][n]);
                    }
                });
        return temp;
    }
    
    static void sqrtEqual(float[][] A)
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[m].length; n++)
                    {
                        A[m][n] = (float) Math.sqrt(A[m][n]);
                    }
                });
    }

    static float[][] T(float[][] A)
    {
        float[][] temp = new float[A[0].length][A.length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = A[n][m];
                    }
                });
        return temp;
    }

    public static float[][] random(int M, int N, Random rnd)
    {
        float[][] temp = new float[M][N];
        for (float[] temp1 : temp)
        {
            for (int n = 0; n < temp1.length; n++)
            {
//                temp[m][n] = Math.random() * 2 - 1;
//                temp[m][n] = (rnd.nextDouble()* 2 - 1)*.5;
                temp1[n] = (float)rnd.nextGaussian();
            }
        }
        nMath.divideEquals(temp, N);
        return temp;
    }

    static float[][] copy(float[][] A)
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
//                    System.arraycopy(A[m], 0, temp[m], 0, A[0].length);
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = A[m][n];
                    }
                });
        return temp;
    }

    public static void printM(float[][] A)
    {
        System.out.print("[");
        for (int m = 0; m < A.length; m++)
        {
            if (m == 0)
            {
                System.out.print("[");
            } else
            {
                System.out.print(" [");
            }
            for (int n = 0; n < A[m].length; n++)
            {
                if (A[m][n] < 0)
                {
                    System.out.printf("%.5f", A[m][n]);
                } else
                {
                    System.out.printf(" " + "%.5f", A[m][n]);
                }

                if (n != A[m].length - 1)
                {
                    System.out.print(",");
                }
            }
            System.out.print("]");
            if (m != A.length - 1)
            {
                System.out.println("");
            }
        }
        System.out.print("]");
        System.out.println("");
    }

    static float getMax(float[] a)
    {
        float max = 0;
        for (float d : a)
        {
            if (d > max)
            {
                max = d;
            }
        }
        if (max == 0.0)
        {
            max = Float.MIN_VALUE;
        }
        return max;
    }

    static float getMin(float[] a)
    {
        float min = Float.MAX_VALUE;
        for (float d : a)
        {
            if (d < min)
            {
                min = d;
            }
        }
        return min;
    }

    public static void normMEqual(float[][] A, int size)
    {
        IntStream.range(0, size)
                .parallel()
                .forEach(m ->
                {
                    float max = getMax(A[m]);
                    for (int n = 0; n < A[0].length; n++)
                    {
                        A[m][n] /=  max;
                    }
                });
    }

    public static float[][] normM(float[][] A)
    {
        float[][] temp = new float[A.length][A[0].length];

        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    float min = getMin(A[m]);
                    float max = getMax(A[m]) - min;
                    for (int n = 0; n < A[0].length; n++)
                    {
                        temp[m][n] = (A[m][n] - min) / max;
                    }
                });
        return temp;
    }

    public static float[][] normN(float[][] A)
    {
        float[][] B = T(A);
        B = normM(B);
        B = T(B);
        return B;
    }

    static float[][] log(float[][] A)
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = (float) Math.log(A[m][n] + Float.MIN_VALUE);
                    }
                });
        return temp;
    }

    static void logEqual(float[][] A)
    {
        Arrays.stream(A)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < m.length; n++)
                    {
                        m[n] = (float) Math.log(m[n] + Float.MIN_VALUE);
                    }
                });
    }

    static void expEqual(float[][] A)
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[0].length; n++)
                    {
                        A[m][n] = (float) Math.exp(A[m][n]);
                    }
                });
    }

    static float[][] exp(float[][] A)
    {
        float[][] temp = new float[A.length][A[0].length];
        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = (float) Math.exp(A[m][n]);
                    }
                });
        return temp;
    }

    static float sum(float[][] A)
    {
        float[] temp = new float[1];
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[m].length; n++)
                    {
                        temp[0] += A[m][n];
                    }
                });
        return temp[0];
    }

    static float[][] sumN(float[][] A)
    {
        float[][] temp = new float[1][A[0].length];
        //sample sayısını teke indirip, bulunan kareleri kolonlamasına toplayıp, sıra sayısının iki katına bölüyoruz.
        IntStream.range(0, temp[0].length)
                .parallel()
                .forEach(n ->
                {
                    for (float[] Aa : A)
                    {
                        temp[0][n] += Aa[n];
                    }
                });
        return temp;
    }

    static float[][] sumM(float[][] A)
    {
        float[][] temp = new float[A.length][1];
        //sample sayısını teke indirip, bulunan kareleri kolonlamasına toplayıp, sıra sayısının iki katına bölüyoruz.
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[m].length; n++)
                    {
                        temp[m][0] += A[m][n];
                    }
                });
        return temp;
    }

    static void addEqualMvector(float[][] A, float[][] B)
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[m].length; n++)
                    {
                        A[m][n] = A[m][n] + B[0][n];
                    }
                });
    }

    static float[][] addMvector(float[][] A, float[][] B)
    {
        float[][] temp = new float[A.length][A[0].length];

        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = A[m][n] + B[0][n];
                    }
                });
        return temp;
    }

    static void subEqualNvector(float[][] A, float[][] B)
    {
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < A[0].length; n++)
                    {
                        A[m][n] = A[m][n] - B[m][0];
                    }
                });
    }

    static float[][] subNvector(float[][] A, float[][] B)
    {
        float[][] temp = new float[A.length][A[0].length];

        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[m].length; n++)
                    {
                        temp[m][n] = A[m][n] - B[m][0];
                    }
                });
        return temp;
    }

    static int[] getMaxIndexM(float[][] A)
    {
        int[] temp = new int[A.length];
        //sample sayısını teke indirip, bulunan kareleri kolonlamasına toplayıp, sıra sayısının iki katına bölüyoruz.
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    float maxVal = 0;
                    for (int n = 0; n < A[0].length; n++)
                    {
                        if (A[m][n] > maxVal)
                        {
                            temp[m] = n;
                            maxVal = A[m][n];
                        }
                    }
                });
        return temp;
    }

    static float[][] getMaxValM(float[][] A)
    {
        float[][] temp = new float[A.length][1];
        //sample sayısını teke indirip, bulunan kareleri kolonlamasına toplayıp, sıra sayısının iki katına bölüyoruz.
        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    float maxVal = 0;
                    for (int n = 0; n < A[0].length; n++)
                    {
                        if (A[m][n] > maxVal)
                        {
                            maxVal = temp[m][0] = A[m][n];
                        }
                    }
                });
        return temp;
    }

    public static float[][] encode(float[][] A)
    {
        List<Float> categoryList = new ArrayList<>();
        categoryList.add(A[0][0]);
        for (int m = 1; m < A.length; m++)
        {
            if (!categoryList.contains(A[m][0]))
            {
                categoryList.add(A[m][0]);
            }
        }
        float[][] temp = new float[A.length][categoryList.size()];

        for (int m = 0; m < A.length; m++)
        {
            temp[m][(int) A[m][0]] = 1;
        }
        return temp;
    }

    public static float[][] drop(float[][] A, int[] M, int[] N)
    {
        float[][] temp = new float[(M.length == 0) ? A.length : M.length][(N.length == 0) ? A[0].length : N.length];

        IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < temp[0].length; n++)
                    {
                        temp[m][n] = A[(M.length == 0) ? m : M[m]][(N.length == 0) ? n : N[n]];
                    }
                });
        return temp;
    }

    public static void recoverDrop(float[][] A, float[][] B,  int[] M, int[] N)
    {
        IntStream.range(0, B.length)
                .parallel()
                .forEach(m ->
                {
                    for (int n = 0; n < B[0].length; n++)
                    {
                        A[(M.length == 0) ? m : M[m]][(N.length == 0) ? n : N[n]] = B[m][n];
                    }
                });
    }
    
}