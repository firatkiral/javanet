
package neuralNet.old;

import java.util.stream.IntStream;
import static neuralNet.old.nMath.*;

/**
 *
 * @author firat
 */
public enum activations
{
    SIGMOID
    {
        @Override
        float[][] fn(float[][] X)
        {
            float[][] temp = new float[X.length][X[0].length];
            IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                for (int n = 0; n < temp[0].length; n++)
                {
                    temp[m][n] = 1f / (1f + (float) Math.exp(X[m][n] * -1f));
                }
            });
            return temp;
        }

        @Override
        void fnEquals(float[][] X)
        {
            for (float[] X1 : X)
            {
                for (int n = 0; n < X[0].length; n++)
                {
                    X1[n] = 1f / (1f + (float) Math.exp(X1[n] * -1f));
                }
            }
        }

        @Override
        float[][] prime(float[][] X, float[][] N)
        {
            return times(X, minus(1, X));
        }
    },
    RELU
    {
        @Override
        float[][] fn(float[][] X)
        {
            float[][] temp = new float[X.length][X[0].length];
            IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                for (int n = 0; n < temp[0].length; n++)
                {
                    temp[m][n] = (X[m][n] < 0) ? 0 : X[m][n];
                }
            });
            return temp;
        }

        @Override
        void fnEquals(float[][] X)
        {
            for (float[] X1 : X)
            {
                for (int n = 0; n < X[0].length; n++)
                {
                    X1[n] = (X1[n] < 0) ? 0 : X1[n];
                }
            }
        }

        @Override
        float[][] prime(float[][] X, float[][] N)
        {
            float[][] temp = copy(N);
            IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                for (int n = 0; n < temp[0].length; n++)
                {
                    temp[m][n] = (N[m][n] < 0) ? 0 : 1;
                }
            });
            return temp;
        }
    },
    TANH
    {
        @Override
        float[][] fn(float[][] X)
        {
            float[][] temp = new float[X.length][X[0].length];
            IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                for (int n = 0; n < temp[0].length; n++)
                {
                    temp[m][n] = 1f / (1f + (float) Math.exp(X[m][n] * -1f));
                }
            });
            return temp;
        }

        @Override
        void fnEquals(float[][] X)
        {
            for (float[] X1 : X)
            {
                for (int n = 0; n < X[0].length; n++)
                {
                    X1[n] = 1f / (1f + (float) Math.exp(X1[n] * -1f));
                }
            }
        }

        @Override
        float[][] prime(float[][] X, float[][] N)
        {
            return times(X, minus(1, X));
        }
    },
    LINEAR
    {
        @Override
        float[][] fn(float[][] X)
        {
            float[][] temp = new float[X.length][X[0].length];
            IntStream.range(0, temp.length)
                .parallel()
                .forEach(m ->
                {
                for (int n = 0; n < temp[0].length; n++)
                {
                    temp[m][n] = 1f / (1f + (float) Math.exp(X[m][n] * -1f));
                }
            });
            return temp;
        }

        @Override
        void fnEquals(float[][] X)
        {
            for (float[] X1 : X)
            {
                for (int n = 0; n < X[0].length; n++)
                {
                    X1[n] = 1f / (1f + (float) Math.exp(X1[n] * -1f));
                }
            }
        }

        @Override
        float[][] prime(float[][] X, float[][] N)
        {
            return times(X, minus(1, X));
        }
    },
    SOFTMAX
    {
        @Override
        float[][] fn(float[][] X)
        {
            final float[][] max = getMaxValM(X);
            subEqualNvector(X, max);
            expEqual(X);
//            plusEquals(X, 0.001f);
            final float[][] sum = sumM(X);

            float[][] temp = new float[X.length][X[0].length];
            for (int m = 0; m < temp.length; m++)
            {
                for (int n = 0; n < temp[0].length; n++)
                {
                    temp[m][n] = X[m][n] / sum[m][0];
                }
            }
            return temp;
        }

        @Override
        void fnEquals(float[][] X)
        {
            /*Assumes x is an instance of the Matrix class
            Subtracting large constant from each of x values to prevent overflow*/
            float[][] max = getMaxValM(X);
            subEqualNvector(X, max);
            expEqual(X);
//            plusEquals(X, 0.001f);
            float[][] sum = sumM(X);
            for (int m = 0; m < X.length; m++)
            {
                for (int n = 0; n < X[0].length; n++)
                {
                    X[m][n] /= sum[m][0];
                }
            }
        }

        @Override
        float[][] prime(float[][] X, float[][] N)
        {
            return times(X, minus(1, X));
        }
    };

    abstract float[][] fn(float[][] X);

    abstract void fnEquals(float[][] X);

    abstract float[][] prime(float[][] X, float[][] N);
}
