
package neuralNet.old;

import static neuralNet.old.nMath.*;

public enum costFunctions
{
    mse
    {
        @Override
        public float cost(float[][] O, float[][] Y)
        {
            float[][] E = minus(O, Y);
            E = times(E, E);

            //burada da elimizde n kolon sayılı errorları toplayıp çıktı olarak gönderiyoruz. 
            return (sum(E) * .5f) / E.length;
        }

        @Override
        public float[][] costPrime(float[][] O, float[][] Y, activations aType)
        {
            return times((minus(O, Y)), aType.prime(O, O));
        }

        @Override
        public float accuracy(float[][] O, float[][] Y)
        {
            float guessDiff = 0;
            for (int m = 0; m < Y.length; m++)
            {
                float guessDiffN = 0;
                for (int n = 0; n < Y[m].length; n++)
                {
                    guessDiffN += Math.abs(Y[m][n] - O[m][n]);
                }
                guessDiffN /= Y[m].length;
                guessDiff += guessDiffN;
            }

            return (guessDiff / Y.length);
        }

    },
    binary_crossentropy
    {
        @Override
        public float cost(float[][] O, float[][] Y)
        {
//            y * log(output) + (1 - y) * log(1 - output)
            float[][] E = plus(times(Y, log(O)), times(minus(1, Y), log(minus(1, O))));

            //burada da elimizde n kolon sayılı errorları toplayıp çıktı olarak gönderiyoruz. 
            return (sum(E) * -1f) / E.length;
        }

        @Override
        public float[][] costPrime(float[][] O, float[][] Y, activations aType)
        {
            return minus(O, Y);
        }

        @Override
        public float accuracy(float[][] O, float[][] Y)
        {
            float trueGuess = 0;
            for (int i = 0; i < Y.length; i++)
            {
                if ((int) (O[i][0] + .5) == Y[i][0])
                {
                    trueGuess++;
                }
            }

            return (trueGuess / Y.length) * 100;
        }
    },
    categorical_crossentropy
    {
        @Override
        public float cost(float[][] O, float[][] Y)
        {
//          y * log(output)
            float[][] E = times(Y, log(O));

            //burada da elimizde n kolon sayılı errorları toplayıp çıktı olarak gönderiyoruz. 
            return (sum(E) * -1f) / E.length;
        }

        @Override
        public float[][] costPrime(float[][] O, float[][] Y, activations aType)
        {
            return minus(O, Y);
        }

        @Override
        public float accuracy(float[][] O, float[][] Y)
        {
            int[] Yidx = getMaxIndexM(Y);
            int[] Oidx = getMaxIndexM(O);
            float trueGuess = 0;
            for (int i = 0; i < Yidx.length; i++)
            {
                if (Yidx[i] == Oidx[i])
                {
                    trueGuess++;
                }
            }

            return (trueGuess / Y.length) * 100;
        }
    };

    public abstract float cost(float[][] O, float[][] Y);

    public abstract float[][] costPrime(float[][] O, float[][] Y, activations aType);

    public abstract float accuracy(float[][] O, float[][] Y);

}
