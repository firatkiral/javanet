
package neuralNet.old;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import static neuralNet.old.nMath.*;

public abstract class layer
{

    public abstract void initializeLayer(Random Rnd, int prevLayerSize);

    public abstract void generateDropoutArray();

    public abstract int getSize();

    public abstract void setRow(int Row);

    public abstract int getRow();

    public abstract void setLayerList(List<layer> LayerList);

    public abstract float[][] getGrads();

    public abstract void setGrads(float[][] Grads);

    public abstract float[][] getPrevWeightsDeltas();

    public abstract float[][] getPrevBiasDeltas();

    public abstract float[][] getPrevWeightsDeltasADAM();

    public abstract float[][] getPrevBiasesDeltasADAM();

    public abstract float[][] getWeights();

    public abstract float[][] getBiases();

    public abstract float[][] getOutputs();

    public abstract int[] dropout(int[] M);
    
    public abstract int[] recoverDropout(int[] M);

    public abstract float[][] feedforward(float[][] inputs);

    public abstract void calcGrad();

}

class fullyConnected extends layer
{

    Random rnd;
    int size;
    float dropout;
    int[] dropArray;

    float[][] weights;
    float[][] biases;
    float[][] prevWeightsDeltas;
    float[][] prevBiasesDeltas;
    float[][] prevWeightsDeltasADAM;
    float[][] prevBiasesDeltasADAM;
    
    float[][] WEIGHTS;
    float[][] BIASES;
    float[][] PREVWEIGHTSDELTAS;
    float[][] PREVBIASESDELTAS;
    float[][] PREVWEIGHTSDELTASADAM;
    float[][] PREVBIASESDELTASADAM;

    float[][] outputs;
    float[][] nets;
    float[][] grads;
    int row = 0;
    List<layer> layerList;

    activations aType;

    public fullyConnected(int Size, activations AType)
    {
        size = Size;
        aType = AType;
        dropArray = new int[0];
    }

    public fullyConnected(int Size, activations AType, float Dropout)
    {
        size = Size;
        aType = AType;
        dropout = 1-Dropout;
    }

    @Override
    public void initializeLayer(Random Rnd, int prevLayerSize)
    {
        rnd = Rnd;
        WEIGHTS = random(prevLayerSize, size, rnd);
        BIASES = random(1, size, rnd);

        PREVWEIGHTSDELTAS = new float[prevLayerSize][size];
        PREVBIASESDELTAS = new float[1][size];
        PREVWEIGHTSDELTASADAM = new float[prevLayerSize][size];
        PREVBIASESDELTASADAM = new float[1][size];
    }

    @Override
    public float[][] getGrads()
    {
        return grads;
    }

    @Override
    public float[][] getWeights()
    {
        return weights;
    }

    @Override
    public float[][] feedforward(float[][] inputs)
    {
        /*                          nöron ağırlıkları
                                  nrn1 nrn2 nrn3  nrn4     3 x 4 layer, 3 nöronlu 2(row) training sample ve
                                [[[w11][w21][w31][w41]]    4 nöronlu sonraki layerin ağırlıklarının çarpımı.
                                 [[w12][w22][w32][w42]]    2. layerda her nöronda 3 ağırlık var, çünkü önceki layerda
                                 [[w13][w23][w33][w43]]]   3 nöron var. Çıkıs row sayısıyıda sample sayısı olan 2.

        giriş inputları           çıkışlar
        [[[o11][o12][o13]]        [[[o11.w11+o12.w12+o13.w13+o14*w14][...][...][...]]
         [[o21][o22][o23]]]        [[o21.w11+o22.w12+o23.w13+o24*w14][...][...][...]]      */

        outputs = dot(inputs, weights);
        /*herhangi bir layerdaki training sample sayısı kadar outputları bias la topluyoruz
         [[[o1][o2][o3]] +[[b1][b2][b3]] sampple sayısı 1den büyükse toplama işlemi için bias ın rowlarını outputa
          [[o1][o2][o3]]]                eşit olacak şekilde topluyoruz.   */
        addEqualMvector(outputs, biases);
        nets = copy(outputs);
        aType.fnEquals(outputs);
        return outputs;
    }

    @Override
    public void calcGrad()
    {
//        float [][] t = new float[layerList.get(row+1).weights[0].length][layerList.get(row+1).weights.length];
//        Tfast(0, layerList.get(row+1).weights.length, 0, layerList.get(row+1).weights[0].length, layerList.get(row+1).weights, t);
//        grads = dot(layerList.get(row+1).grads, t);
        grads = dotT(layerList.get(row + 1).getGrads(), layerList.get(row + 1).getWeights());
//        grads = dot(layerList.get(row+1).grads, T(layerList.get(row+1).weights));
        timesEquals(grads, aType.prime(outputs, nets));
    }

    @Override
    public int getRow()
    {
        return row;
    }

    @Override
    public void setRow(int Row)
    {
        row = Row;
    }

    @Override
    public void setLayerList(List<layer> LayerList)
    {
        layerList = LayerList;
    }

    @Override
    public int getSize()
    {
        return size;
    }

    @Override
    public float[][] getOutputs()
    {
        return outputs;
    }

    @Override
    public void setGrads(float[][] Grads)
    {
        grads = Grads;
    }

    @Override
    public float[][] getPrevWeightsDeltas()
    {
        return prevWeightsDeltas;
    }

    @Override
    public float[][] getPrevBiasDeltas()
    {
        return prevBiasesDeltas;
    }

    @Override
    public float[][] getBiases()
    {
        return biases;
    }

    @Override
    public float[][] getPrevWeightsDeltasADAM()
    {
        return prevWeightsDeltasADAM;
    }

    @Override
    public float[][] getPrevBiasesDeltasADAM()
    {
        return prevBiasesDeltasADAM;
    }

    @Override
    public void generateDropoutArray()
    {
        dropArray = new int[(int) (size * dropout)];
        List<Integer> dropList = new ArrayList<>();
        for (int i = 0; i < size; i++)
        {
            dropList.add(i);
        }
//        Collections.shuffle(dropList, rnd);
        for (int i = 0; i < size - dropArray.length; i++)
        {
            dropList.remove(rnd.nextInt(dropList.size()));
        }
        for (int i = 0; i < dropArray.length; i++)
        {
            dropArray[i] = dropList.get(i);
        }
    }

    @Override
    public int[] dropout(int[] M)
    {
        generateDropoutArray();
        
        if (M.length > 0 || dropArray.length > 0)
        {
            weights = drop(WEIGHTS, M, dropArray);
            prevWeightsDeltas = drop(PREVWEIGHTSDELTAS, M, dropArray);
            prevWeightsDeltasADAM = drop(PREVWEIGHTSDELTASADAM, M, dropArray);
            
            biases = drop(BIASES, new int[] {0} ,dropArray);
            prevBiasesDeltas = drop(PREVBIASESDELTAS, new int[] {0} ,dropArray);
            prevBiasesDeltasADAM = drop(PREVBIASESDELTASADAM, new int[] {0} ,dropArray);
        }
        else
        {
            weights = WEIGHTS;
            biases = BIASES;
            prevWeightsDeltas = PREVWEIGHTSDELTAS;
            prevBiasesDeltas = PREVBIASESDELTAS;
            prevWeightsDeltasADAM = PREVWEIGHTSDELTASADAM;
            prevBiasesDeltasADAM = PREVBIASESDELTASADAM;
        }
        
        return dropArray;
        
    }

    @Override
    public int[] recoverDropout(int[] M)
    {
        if (M.length > 0 || dropArray.length > 0)
        {
            recoverDrop(WEIGHTS, weights, M, dropArray);
            recoverDrop(PREVWEIGHTSDELTAS, prevWeightsDeltas, M, dropArray);
            recoverDrop(PREVWEIGHTSDELTASADAM, prevWeightsDeltasADAM, M, dropArray);
            
            recoverDrop(BIASES, biases, new int[] {0}, dropArray);
            recoverDrop(PREVBIASESDELTAS, prevBiasesDeltas, new int[] {0}, dropArray);
            recoverDrop(PREVBIASESDELTASADAM, prevBiasesDeltasADAM, new int[] {0}, dropArray);
            
            weights = WEIGHTS;
            biases = BIASES;
            prevWeightsDeltas = PREVWEIGHTSDELTAS;
            prevBiasesDeltas = PREVBIASESDELTAS;
            prevWeightsDeltasADAM = PREVWEIGHTSDELTASADAM;
            prevBiasesDeltasADAM = PREVBIASESDELTASADAM;
            
        }
        
        return dropArray;
    }

}
