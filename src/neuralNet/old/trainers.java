
package neuralNet.old;

import static neuralNet.old.nMath.*;


public enum trainers
{
    sgd
    {
        @Override
        public void update(nnet n, layer l, float [][] inputs)
        {
            if(n.doDecay) 
            {
                n.doDecay = false;
                n.lRate = n.lRate/(1+n.decay);
                System.out.println("lr= " + n.lRate);
            }
            float[][] grads = l.getGrads();
            
            float[][] WD = dot(T(inputs), grads);
            divideEquals(WD, grads.length);
            timesEquals(WD, n.lRate);
            
            float[][] BD = sumN(l.getGrads());
            divideEquals(BD, grads.length);
            timesEquals(BD, n.lRate);
            
            float [][] prevWeightsDeltas = l.getPrevWeightsDeltas();
            float [][] prevBiasesDeltas = l.getPrevBiasDeltas();
            
            if(n.nesterov)
            {
//                v_prev = v # back this up
//                v = mu * v - learning_rate * dx # velocity update stays the same
//                x += -mu * v_prev + (1 + mu) * v # position update changes form
//                veya 
//                x += v + mu*(v-vprev)
//                http://cs231n.github.io/neural-networks-3/#sgd
                
                float[][] v_prev = copy(prevWeightsDeltas);
                timesEquals(prevWeightsDeltas, n.momentum);
                plusEquals(prevWeightsDeltas, WD);
                timesEquals(v_prev, n.momentum*-1);
                plusEquals(v_prev, times(prevWeightsDeltas, (n.momentum+1)));
                minusEquals(l.getWeights(), v_prev);
                
                float[][] vb_prev = copy(prevBiasesDeltas);
                timesEquals(prevBiasesDeltas, n.momentum);
                plusEquals(prevBiasesDeltas, BD);
                timesEquals(vb_prev, n.momentum*-1);
                plusEquals(vb_prev, times(prevBiasesDeltas, (n.momentum+1)));
                minusEquals(l.getBiases(), vb_prev);
            }
            else
            {
                timesEquals(prevWeightsDeltas, n.momentum);
                plusEquals(WD, prevWeightsDeltas);
                minusEquals(l.getWeights(), WD);
                prevWeightsDeltas = WD;

                timesEquals(prevBiasesDeltas, n.momentum);
                plusEquals(BD, prevBiasesDeltas);
                minusEquals(l.getBiases(), BD);
                prevBiasesDeltas = BD;
            }
        }
    },
    adagrad
    {
        @Override
        public void update(nnet n, layer l, float [][] inputs)
        {
            float[][] grads = l.getGrads();
            float[][] prevWeightsDeltas = l.getPrevWeightsDeltas();
            float[][] prevBiasesDeltas = l.getPrevBiasDeltas();
            float[][] WD = dot(T(inputs), grads);
            divideEquals(WD, grads.length);
            plusEquals(prevWeightsDeltas, times(WD, WD));
            timesEquals(WD, divide(n.lRate, sqrt(plus(prevWeightsDeltas, 0.000001f))));
            minusEquals(l.getWeights(), WD);
            
            float[][] BD = sumN(grads);
            divideEquals(BD, grads.length);
            timesEquals(prevBiasesDeltas, times(BD, BD));
            timesEquals(BD, divide(n.lRate, sqrt(plus(prevBiasesDeltas, 0.000001f))));
            minusEquals(l.getBiases(), BD);
        }
    },
    adam
    {
        @Override
        public void update(nnet n, layer l, float [][] inputs)
        {
            float[][] grads = l.getGrads();
            float[][] prevWeightsDeltas = l.getPrevWeightsDeltas();
            float[][] prevBiasesDeltas = l.getPrevBiasDeltas();
            float[][] prevWeightsDeltasADAM = l.getPrevWeightsDeltasADAM();
            float[][] prevBiasesDeltasADAM = l.getPrevBiasesDeltasADAM();
            
            float[][] WD = dot(T(inputs), grads);
            divideEquals(WD, grads.length);
            timesEquals(prevWeightsDeltas, n.decay);
            plusEquals(prevWeightsDeltas, times(WD, (1-n.decay)));
            float d = (float)Math.pow(n.decay, n.epoc);
            float[][] mt = divide(prevWeightsDeltas, (1-d));
            timesEquals(prevWeightsDeltasADAM, n.decay2);
            timesEquals(WD, WD);
            plusEquals(prevWeightsDeltasADAM, times(WD, (1-n.decay2)));
            float d2 = (float)Math.pow(n.decay2, n.epoc);
            float[][] vt = divide(prevWeightsDeltasADAM, (1-d2));
            sqrtEqual(vt);
            timesEquals(mt, divide(n.lRate, plus(vt, 0.000001f)));
            minusEquals(l.getWeights(), mt);
            
            float[][] BD = sumN(grads);
            divideEquals(BD, grads.length);
            timesEquals(prevBiasesDeltas, n.decay);
            plusEquals(prevBiasesDeltas, times(BD, (1-n.decay)));
            mt = divide(prevBiasesDeltas, (1-d));
            timesEquals(prevBiasesDeltasADAM, n.decay2);
            timesEquals(BD, BD);
            plusEquals(prevBiasesDeltasADAM, times(BD, (1-n.decay2)));
            vt = divide(prevBiasesDeltasADAM, (1-d2));
            sqrtEqual(vt);
            timesEquals(mt, divide(n.lRate, plus(sqrt(vt), 0.000001f)));
            minusEquals(l.getBiases(), mt);
        }
    };

    public abstract void update(nnet n, layer l, float [][] inputs);

}
