
package neuralNet.old;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class nnet
{

    private final Random rnd;
    int row = 0;

    float[][] trainX;
    float[][] trainY;
    float[][] testX;
    float[][] testY;

    //batch attributes -------
    float[][] batchTrainX;
    float[][] batchTrainY;
    int batchSize;
    int batchCount;
//    int batchRemainder;
    //----------------------------

    int layersSize;
    float costVal;
    public costFunctions costFunc;
    activations aType;

    List<Integer> shuffleList;

    public List<layer> layerList = new ArrayList<>();

    public trainers trainer;
    //optimizers
    //-------------------------
    float momentum;
    float lRate;
    float decay;
    float decay2;
    boolean doDecay;
    boolean nesterov;
    int epoc = 1;
    //-------------------------

    public nnet(float[][] trainX, float[][] trainY, int batchSize, Random rnd)
    {
        this.rnd = rnd;
        this.trainX = trainX;
        this.trainY = trainY;

        this.batchSize = batchSize;
        this.batchCount = trainX.length - (trainX.length % batchSize);
        this.batchTrainX = new float[batchSize][];
        this.batchTrainY = new float[batchSize][];

//        layerList.add(new inputLayer(trainX[1].length, batchSize));
    }

    public nnet(float[][] trainX, float[][] trainY, int batchSize, Random rnd, float dropout)
    {
        this.rnd = rnd;
        this.trainX = trainX;
        this.trainY = trainY;

        this.batchSize = batchSize;
        this.batchCount = trainX.length - (trainX.length % batchSize);
        this.batchTrainX = new float[batchSize][];
        this.batchTrainY = new float[batchSize][];

//        layerList.add(new inputLayer(trainX[1].length, dropout));
    }

    public void addLayer(int layerSize, activations aType)
    {
        layerList.add(new fullyConnected(layerSize, aType));
        this.aType = aType;//her layer eklendiğinde üstüne yazıyor, sonuncu kalıyor. Bakılacak.
    }

    public void addLayer(int layerSize, activations aType, float dropout)
    {
        layerList.add(new fullyConnected(layerSize, aType, dropout));
        this.aType = aType;//her layer eklendiğinde üstüne yazıyor, sonuncu kalıyor. Bakılacak.
    }

    public void addSGDtrainer()
    {
        this.trainer = trainers.sgd;
        this.momentum = 0.7f;
        this.lRate = 0.15f;
        this.decay = 0.001f;
        this.nesterov = false;
    }

    public void addSGDtrainer(float lRate, float momentum, float decay, boolean nesterov)
    {
        this.trainer = trainers.sgd;
        this.momentum = momentum;
        this.lRate = lRate;
        this.decay = decay;
        this.nesterov = nesterov;
    }

    public void addADAGRADtrainer()
    {
        this.trainer = trainers.adagrad;
        this.lRate = .01f;
    }

    public void addADAGRADtrainer(float lRate)
    {
        this.trainer = trainers.adagrad;
        this.lRate = lRate;
    }

    public void addADAMtrainer()
    {
        this.trainer = trainers.adam;
        this.lRate = .01f;
        this.decay = .9f;
        this.decay2 = .999f;
    }

    public void addADAMtrainer(float lRate, float decay, float decay2)
    {
        this.trainer = trainers.adam;
        this.lRate = lRate;
        this.decay = decay;
        this.decay2 = decay2;
    }

    public void build(costFunctions costFunc) throws IOException
    {
        layersSize = layerList.size();
        this.costFunc = costFunc;

        shuffleList = new ArrayList<>();
        for (int i = 0; i < trainX.length; i++)
        {
            shuffleList.add(i);
        }

        int size = trainX[0].length; 
        for (int i = 0; i < layersSize; i++)
        {
            layerList.get(i).setRow(i);
            layerList.get(i).setLayerList(layerList);
            layerList.get(i).initializeLayer(rnd, size);
            size = layerList.get(i).getSize();
        }

    }
    
    void dropout()
    {
        int [] M = new int [0];
        
        for (int i = 0; i < layersSize; i++)
        {
            M = layerList.get(i).dropout(M);
        }
    }
    
    void recoverDropout()
    {
        int [] M = new int [0];
        
        for (int i = 0; i < layersSize; i++)
        {
            M = layerList.get(i).recoverDropout(M);
        }
    }

    void shuffle()
    {
        Collections.shuffle(shuffleList, rnd);
    }

    public void train()
    {
        costVal = 0;
        shuffle();
        for (int i = 0; i < batchCount; i++)
        {
            batchTrainX[i % batchSize] = trainX[shuffleList.get(i)];
            batchTrainY[i % batchSize] = trainY[shuffleList.get(i)];
            if (i % batchSize == batchSize - 1)
            {
                dropout();
                feedForward(batchTrainX);
                backProp();
                recoverDropout();
            }
        }
//        if (batchRemainder > 0)
//        {
//            float[][] batchTrainXremainder = new float[batchRemainder][];
//            float[][] batchTrainYremainder = new float[batchRemainder][];
//
//            for (int i = 0; i < batchRemainder; i++)
//            {
//                batchTrainXremainder[i] = trainX[shuffleList.get(batchCount + i)];
//                batchTrainYremainder[i] = trainY[shuffleList.get(batchCount + i)];
//            }
//
//            feedForward(batchTrainXremainder);
//            backProp(batchTrainYremainder);
//        }
    }

    public void feedForward(float[][] batchTrainX)
    {
        float [][] OUTPUTS = batchTrainX;

        //her layer için for loop. Batch sayısı kadar sample için nMath yardımıyla çıkışlar 1 kerede hesaplanıyor.
        for (int i = 0; i < layersSize; i++)
        {
           OUTPUTS = layerList.get(i).feedforward(OUTPUTS);
        }
    }


    void backProp()
    {
        /* network output sayısı 2 olarak düşünürsek her batch sample için eroru buluyoruz
           e = output - istenen output
           sample 1 [[[e11][e12]]
           sample 2  [[e21][e22]]]
         */
//        float[][] E = minus((layerList.get(layerList.layersSize() - 1).getOutputs()),(Y));
        //seçilen cost fonksiyonuna göre, çıkışlarla istenen çıkışlar arasındaki mesafe ölçülüyor
        costVal += costFunc.cost((layerList.get(layerList.size() - 1).getOutputs()), (batchTrainY));
//        float[][] grads = times(E, SigmoidActivation.prime(layerList.get(layerList.layersSize() - 1).getOutputs()));

        layerList.get(layersSize - 1).setGrads(costFunc.costPrime(layerList.get(layersSize - 1).getOutputs(), batchTrainY, aType));
        for (int i = layersSize - 2; i >= 0; i--)
        {
            layerList.get(i).calcGrad();
        }
        
        float [][] OUTPUTS = batchTrainX;
        for (int i = 0; i < layersSize; i++)
        {
            trainer.update(this, layerList.get(i), OUTPUTS);
            OUTPUTS = layerList.get(i).getOutputs();
        }

    }

}
