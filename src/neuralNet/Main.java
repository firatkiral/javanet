package neuralNet;


public class Main {
    public static void main(String[] args) {
        int[] shape = new int[]{3 ,4, 2};
        Tensor a = new Tensor(24);
        TensorHelper.reshape(a, shape);
        TensorHelper.createTestDefaults(a);
        TensorHelper.print(a);

        //4 3 2
        shape = new int[]{2, 4, 3};
//        shape = new int[]{4, 3, 2};
        Tensor b = new Tensor(24);
        TensorHelper.reshape(b, shape);
        TensorHelper.createTestDefaults(b);
        TensorHelper.print(b);

        System.out.println(TensorHelper.getValue(b, new int[]{0, 0, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 0, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 0, 2}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 1, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 1, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 1, 2}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 2, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 2, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 2, 2}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 3, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 3, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 3, 2}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 0, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 0, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 0, 2}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 1, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 1, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 1, 2}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 2, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 2, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 2, 2}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 3, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 3, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 3, 2}));

        int[] shp = b.shape;
        TensorHelper.permute(b, new int[]{1, 2, 0});
        shp = b.shape;

        System.out.println(TensorHelper.getValue(b, new int[]{0, 0, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 1, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 2, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 0, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 1, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 2, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{2, 0, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{2, 1, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{2, 2, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{3, 0, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{3, 1, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{3, 2, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 0, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 1, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 2, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 0, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 1, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 2, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{2, 0, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{2, 1, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{2, 2, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{3, 0, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{3, 1, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{3, 2, 1}));

        System.out.println(TensorHelper.getValue(b, new int[]{0, 0, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 0, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 1, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 1, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 2, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 2, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 0, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 0, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 1, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 1, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 2, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 2, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{2, 0, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{2, 0, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{2, 1, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{2, 1, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{2, 2, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{2, 2, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{3, 0, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{3, 0, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{3, 1, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{3, 1, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{3, 2, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{3, 2, 1}));

//        TensorHelper._bakePermutation(b);
        TensorHelper.print(b);
        System.out.println(TensorHelper.getValue(b, new int[]{0, 0, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 0, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 1, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 1, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 2, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{0, 2, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 0, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 0, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 1, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 1, 1}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 2, 0}));
        System.out.println(TensorHelper.getValue(b, new int[]{1, 2, 1}));
//        TensorHelper.createTestDefaults(b);

        TensorHelper.print(b);


        Tensor c = TensorHelper.dot(a, b);
        TensorHelper.print(c);

//        TensorHelper.permute(b, new int[]{0, 2, 1});
//
//        TensorHelper.multiply(a, b, a);
//        TensorHelper.print(a);
    }
}
