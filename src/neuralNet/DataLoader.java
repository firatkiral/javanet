
package neuralNet;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.util.stream.Stream;


public class DataLoader {


    public static void load(String filePath, String delimiter, Random rnd, boolean shfl) {
        filePath = ClassLoader.getSystemClassLoader().getResource(filePath).getFile();
        Tensor tensor;
        try {

            LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(filePath));
            int lineLength = lineNumberReader.readLine().split(delimiter).length;

            lineNumberReader.skip(Long.MAX_VALUE);
            int lineCount = lineNumberReader.getLineNumber();

            tensor = new Tensor(lineCount * lineLength);

            try (Stream<String> lines = Files.lines(Paths.get(filePath), Charset.defaultCharset())) {
                if (shfl) {
                    lines.forEach(line -> process(line, delimiter, tensor));
                } else {
                    lines.forEachOrdered(line -> process(line, delimiter, tensor));
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void process(String s, String delimiter, Tensor tensor) {
        if (s != null) {
            String[] strArray = s.split(delimiter);
            float[] floatArray = new float[strArray.length];
            for (int i = 0; i < strArray.length; i++) {
                floatArray[i] = Float.parseFloat(strArray[i]);
            }
        }
    }
}
