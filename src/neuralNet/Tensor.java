package neuralNet;

import java.util.Arrays;

public class Tensor {

    int[] shape = new int[]{1};
    int[] permutation = new int[0];
    boolean permuted = false;
    int size;
    final float[] array;

    public Tensor(int size) {
        array = new float[size];
        this.size = size;
    }
}
