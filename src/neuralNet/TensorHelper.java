package neuralNet;

public class TensorHelper {

    //it always bakes to new array
    public static void reshape(Tensor tensor, int[] shape) {
        if (shape == null) {
            System.out.println("shape cannot be null");
        }

        if (shape.length > 3) {
            System.out.println("shape length must be between 1 and 3");
        }

        int size = 1;
        for (int i = 0; i < shape.length; i++) {
            if (shape[i] < 1) {
                System.out.println("shape cannot be 0");
            }
            size *= shape[i];
        }

        if (tensor.size != size) {
            System.out.println("size must be same");
        }

        _bakePermutation(tensor);

        tensor.shape = shape;
        tensor.permutation = new int[shape.length];
        for (int i = 0; i < tensor.permutation.length; i++) {
            tensor.permutation[i] = i;
        }
    }

    public static void permute(Tensor tensor, int[] permutation) {
        if (permutation == null) {
            System.out.println("permutation cannot be null");
            return;
        }

        int len = tensor.permutation.length;
        if (len > 3) {
            System.out.println("permutation length must be between 1 and 3");
            return;
        }

        if (len != permutation.length) {
            System.out.println("permutation length must be same");
            return;
        }

        //convert permutation back to default before permutation applied
        int[] p = depermute(tensor.permutation);
        //convert shape back to default before permutation applied
        int[] shape = permute(tensor.shape, p);

        //apply new permutation
        p = permute(permutation, p);

        //apply new permutation to shape
        tensor.shape = permute(shape, p);

        //keep permutation
        p = depermute(p);
        tensor.permutation = p;


        _checkIfPermuted(tensor);
    }

    public static float getValue(Tensor tensor, int[] pos) {
        switch (pos.length) {
            case 0:
                System.out.println("index array cannot be zero size");
                return 0;
            case 1:
                return tensor.array[pos[0]];
            case 2:
                return getValue2D(tensor, pos);
            case 3:
                return getValue3D(tensor, pos);
            default:
                return getValueND(tensor, pos);

        }
    }

    private static float getValueND(Tensor tensor, int[] pos) {
        return tensor.array[getRowND(tensor, pos, 0)];
    }

    private static float getValue2D(Tensor tensor, int[] pos) {
        return tensor.array[getRow2D(tensor, pos)];
    }

    private static float getValue3D(Tensor tensor, int[] pos) {
        return tensor.array[getRow3D(tensor, pos)];
    }

    public static void setValue(Tensor tensor, int[] pos, float val) {
        switch (pos.length) {
            case 0:
                System.out.println("index array cannot be zero size");
            case 1:
                tensor.array[pos[0]] = val;
            case 2:
                setValue2D(tensor, pos, val);
            case 3:
                setValue3D(tensor, pos, val);
            default:
                setValueND(tensor, pos, val);

        }
    }

    private static void setValueND(Tensor tensor, int[] pos, float val) {
        tensor.array[getRowND(tensor, pos, 0)] = val;
    }

    private static void setValue2D(Tensor tensor, int[] pos, float val) {
        tensor.array[getRow2D(tensor, pos)] = val;
    }

    private static void setValue3D(Tensor tensor, int[] pos, float val) {
        tensor.array[getRow3D(tensor, pos)] = val;
    }

    private static int[] permute(int[] arr, int[] permutation) {
        int[] out = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            out[i] = arr[permutation[i]];
        }
        return out;
    }

    // converts permutation array to backward so
    // if we apply this backward array to any permuted array
    // it reverts permuted array back to its default
    public static int[] depermute(int[] permutation) {
        int[] out = new int[permutation.length];

        for (int i = 0; i < permutation.length; i++) {
            out[permutation[i]] = i;
        }
        return out;
    }

    //for m*n*d shape idx[i,j,k] = i*n*d + j*d + k
    //we are reverting idx and shape back to default if there is any change so above formula works all the time
    private static int getRowND(Tensor tensor, int[] idx, int depth) {
        int index = idx[tensor.permutation[depth]];
        for (int i = ++depth; i < idx.length; i++) {
            index *= tensor.shape[tensor.permutation[i]];
        }

        if (idx.length > depth) {
            index += getRowND(tensor, idx, depth);
        }

        return index;
    }

    private static int getRow2D(Tensor tensor, int[] idx) {
        int i = idx[tensor.permutation[0]];
        int j = idx[tensor.permutation[1]];
        int n = tensor.shape[tensor.permutation[1]];

        return i * n + j;
    }

    private static int getRow3D(Tensor tensor, int[] idx) {
        int i = idx[tensor.permutation[0]];
        int j = idx[tensor.permutation[1]];
        int k = idx[tensor.permutation[2]];
        int n = tensor.shape[tensor.permutation[1]];
        int d = tensor.shape[tensor.permutation[2]];

        return i * n * d + j * d + k;
    }

    public static void print(Tensor tensor) {
        System.out.print("[");
        for (int i = 0; i < tensor.array.length; i++) {
            System.out.print(tensor.array[i] + ",");
        }
        System.out.print("]");
        System.out.println();
    }

    public static void createTestDefaults(Tensor tensor) {
        for (int n = 0; n < tensor.array.length; n++) {
            tensor.array[n] = (float) n;
        }
    }

    // reshape helper
    private static void _bakePermutation(Tensor tensor) {
        if (tensor.permuted) {
            int size = tensor.size;
            float[] newArray = new float[size];

            int[] idx = new int[tensor.shape.length];
            for (int i = 0; i < size; i++) {
                newArray[i] = getValue(tensor, idx);
                getNextIndex(tensor.shape, 0, idx);
            }
            System.arraycopy(newArray, 0, tensor.array, 0, newArray.length);
            tensor.permuted = false;

            for (int i = 0; i < tensor.permutation.length; i++) {
                tensor.permutation[i] = i;
            }
        }
    }

    // reshape helper
    // this returns next index of given ndim array, stars from given idx
    private static int getNextIndex(int[] shape, int depth, int[] idx) {
        int len = shape.length - 1;
        int mod;
        if (depth < len) {
            //we are still not on the last dimension
            mod = getNextIndex(shape, depth + 1, idx);
        } else if (depth == len) {
            //we are on last dimension
            mod = 1;
        } else {
            //it's higher than shape dimension
            return 0;
        }

        int newVal = idx[depth] + mod;
        idx[depth] = newVal % shape[depth];

        return newVal / shape[depth];
    }

    private static void _checkIfPermuted(Tensor tensor) {
        for (int i = 0; i < tensor.permutation.length; i++) {
            if (tensor.permutation[i] != i) {
                tensor.permuted = true;
                return;
            }
        }

        tensor.permuted = false;
    }

    /*******************************************************************************************************************

     MATH
     ******************************************************************************************************************/
    public static void multiply(Tensor A, Tensor B, Tensor C) {
        if (A.shape.length != B.shape.length || A.shape.length != C.shape.length) {
            System.out.println("all tensors must have same dimension");
            return;
        }
        for (int i = 0; i < A.shape.length; i++) {
            if (A.shape[i] != B.shape[i] || A.shape[i] != C.shape[i]) {
                System.out.println("all tensors must have same shape");
                return;
            }
        }

        int len = A.shape.length;
        if (len == 1) {
            _multiply1D(A, B, C);
        } else if (len == 2) {
            _multiply2D(A, B, C);
        } else if (len == 3) {
            _multiply3D(A, B, C);
        } else {
            _multiplyND(A, B, C, new int[len], 0);
        }
    }

    private static void _multiplyND(Tensor A, Tensor B, Tensor C, int[] idx, int depth) {
        for (int i = 0; i < A.shape[depth]; i++) {
            idx[depth] = i;
            if (A.shape.length > depth + 1) {
                _multiplyND(A, B, C, idx, depth + 1);
            } else {
                int row = getRowND(A, idx, 0);
                C.array[row] = A.array[row] * B.array[row];
            }
        }
    }

    private static void _multiply1D(Tensor A, Tensor B, Tensor C) {
        for (int i = 0; i < A.shape[0]; i++) {
            C.array[i] = A.array[i] * B.array[i];
        }
    }

    private static void _multiply2D(Tensor A, Tensor B, Tensor C) {
        int[] idx = new int[2];
        for (int i = 0; i < A.shape[0]; i++) {
            for (int j = 0; j < A.shape[1]; j++) {
                idx[0] = i;
                idx[1] = j;
                int row = getRow2D(A, idx);
                C.array[row] = A.array[row] * B.array[row];
            }
        }
    }

    private static void _multiply3D(Tensor A, Tensor B, Tensor C) {
        int[] idx = new int[3];
        for (int i = 0; i < A.shape[0]; i++) {
            for (int j = 0; j < A.shape[1]; j++) {
                for (int k = 0; k < B.shape[1]; k++) {
                    idx[0] = i;
                    idx[1] = j;
                    idx[2] = k;
                    int row = getRow3D(A, idx);
                    C.array[row] = A.array[row] * B.array[row];
                }
            }
        }
    }

    public static void add(Tensor A, Tensor B, Tensor C) {
        if (A.shape.length != B.shape.length || A.shape.length != C.shape.length) {
            System.out.println("all tensors must have same dimension");
            return;
        }
        for (int i = 0; i < A.shape.length; i++) {
            if (A.shape[i] != B.shape[i] || A.shape[i] != C.shape[i]) {
                System.out.println("all tensors must have same shape");
                return;
            }
        }

        int len = A.shape.length;
        if (len == 1) {
            _add1D(A, B, C);
        } else if (len == 2) {
            _add2D(A, B, C);
        } else if (len == 3) {
            _add3D(A, B, C);
        } else {
            _addND(A, B, C, new int[len], 0);
        }
    }

    private static void _addND(Tensor A, Tensor B, Tensor C, int[] idx, int depth) {
        for (int i = 0; i < A.shape[depth]; i++) {
            idx[depth] = i;
            if (A.shape.length > depth + 1) {
                _addND(A, B, C, idx, depth + 1);
            } else {
                int row = getRowND(A, idx, 0);
                C.array[row] = A.array[row] + B.array[row];
            }
        }
    }

    private static void _add1D(Tensor A, Tensor B, Tensor C) {
        for (int i = 0; i < A.shape[0]; i++) {
            C.array[i] = A.array[i] + B.array[i];
        }
    }

    private static void _add2D(Tensor A, Tensor B, Tensor C) {
        int[] idx = new int[2];
        for (int i = 0; i < A.shape[0]; i++) {
            for (int j = 0; j < A.shape[1]; j++) {
                idx[0] = i;
                idx[1] = j;
                int row = getRow2D(A, idx);
                C.array[row] = A.array[row] + B.array[row];
            }
        }
    }

    private static void _add3D(Tensor A, Tensor B, Tensor C) {
        int[] idx = new int[3];
        for (int i = 0; i < A.shape[0]; i++) {
            for (int j = 0; j < A.shape[1]; j++) {
                for (int k = 0; k < B.shape[1]; k++) {
                    idx[0] = i;
                    idx[1] = j;
                    idx[2] = k;
                    int row = getRow3D(A, idx);
                    C.array[row] = A.array[row] + B.array[row];
                }
            }
        }
    }

    //always return new tensor  A = m x n B = n x d return C = m x n
    public static Tensor dot(Tensor A, Tensor B) {
        if (A.shape.length < 2 || B.shape.length < 2) {
            System.out.println("Tensors must have at least 2 dimension");
            return null;
        }
        if (A.shape.length != B.shape.length) {
            System.out.println("all tensors must have same dimension");
            return null;
        }
        if (A.shape[1] != B.shape[0]) {
            System.out.println("A's column size must be same with B's row size");
            return null;
        }

        int size = A.shape[0] * B.shape[1];
        int[] shape = new int[A.shape.length];
        shape[0] = A.shape[0];
        shape[1] = B.shape[1];
        if (A.shape.length > 2) {
            for (int i = 2; i < A.shape.length; i++) {
                if (A.shape[i] != B.shape[i]) {
                    System.out.println("Tensors must have same size on each dimension after second dimension");
                    return null;
                }
                size *= A.shape[i];
                shape[i] = A.shape[i];
            }
        }

        Tensor C = new Tensor(size);
        reshape(C, shape);

        if (shape.length == 2) _dot2D(A, B, C);
        else _dot(A, B, C);

        return C;
    }

    private static void _dot(Tensor A, Tensor B, Tensor C) {
        int[] idx = new int[A.shape.length];
        int size = C.size / (A.shape[0] * B.shape[1]);
        for (int i = 0; i < A.shape[0]; i++) {
            for (int k = 0; k < A.shape[1]; k++) {
                for (int j = 0; j < B.shape[1]; j++) {
                    for (int l = 0; l < size; l++) {
                        idx[0] = i;
                        idx[1] = k;
                        float a = getValue(A, idx);
                        idx[0] = k;
                        idx[1] = j;
                        float b = getValue(B, idx);
                        idx[0] = i;
                        //idx[1] = j; it's already assigned to j
                        float c = getValue(C, idx);
                        setValue(C, idx, c + a * b);
                        getNextIndex(A.shape, 2, idx);
                    }
                }
            }
        }
    }

    private static void _dot2D(Tensor A, Tensor B, Tensor C) {
        int[] idx = new int[2];
        for (int i = 0; i < A.shape[0]; i++) {
            for (int k = 0; k < A.shape[1]; k++) {
                for (int j = 0; j < B.shape[1]; j++) {
                    idx[0] = i;
                    idx[1] = k;
                    float a = getValue2D(A, idx);
                    idx[0] = k;
                    idx[1] = j;
                    float b = getValue2D(B, idx);
                    idx[0] = i;
                    //idx[1] = j; it's already assigned to j
                    float c = getValue2D(C, idx);
                    setValue2D(C, idx, c + a * b);
                }
            }
        }
    }
}
