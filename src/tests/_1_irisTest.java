
package tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import neuralNet.old.activations;
import neuralNet.old.costFunctions;
import neuralNet.old.dataReader;
import static neuralNet.old.nMath.*;
import neuralNet.old.nnet;

public class _1_irisTest
{
    static float [][] trX;
    static float [][] trY;
    static float [][] tsX;
    static float [][] tsY;
    
    static void initializeData(int ySplit, double testSplit)
    {
        int dataCount = dataReader.dataset.length;
//        dataReader.dataset = normN(dataReader.dataset, dataReader.dataset[0].length-1);
        int testCount = (int) (dataReader.dataset.length*testSplit);
        
        trX = new float[dataCount-testCount][];
        trY = new float[dataCount-testCount][];
        tsX = new float[testCount][];
        tsY = new float[testCount][];
        
        for (int i = 0; i < dataCount; i++)
        {
            if (trX.length > i)
            {
                trX[i] = Arrays.copyOfRange(dataReader.dataset[i], 0, ySplit);
                trY[i] = Arrays.copyOfRange(dataReader.dataset[i], ySplit, dataReader.dataset[i].length);
            }
            else
            {
                tsX[i-trX.length] = Arrays.copyOfRange(dataReader.dataset[i], 0, ySplit);
                tsY[i-trX.length] = Arrays.copyOfRange(dataReader.dataset[i], ySplit, dataReader.dataset[i].length);
            }
        }
        dataReader.dataset = new float[0][0];
    }
    
//    static private Random rnd = new Random(System.currentTimeMillis());
    static private Random rnd = new Random(5);
    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        dataReader.read("datasets/iris.out", " ", rnd, true);
        initializeData(4, .2f);
        
        //encode problemi, shuffle olmadigi icin tsY de sadece 2 ler var
        trY = encode(trY);
        tsY = encode(tsY);

        int batchSize = 6;
        int batchCount = trX.length - (trX.length%batchSize);
        
        nnet nnet = new nnet(trX, trY, batchSize, rnd);
        nnet.addLayer(18, activations.RELU, .1f);
        nnet.addLayer(3, activations.SOFTMAX);
        nnet.addSGDtrainer(.01f, .1f, .0001f, true);
//        nnet.addADAGRADtrainer();
//        nnet.addADAMtrainer();
        nnet.build(costFunctions.categorical_crossentropy);

        long startTime = System.currentTimeMillis();

        for (int i = 0; i < 200; i++)
        {
            long startBatchTime = System.currentTimeMillis();
            nnet.train();
            if (i % 20 == 0)
            {
                System.out.println("epoc: "+ i + " ----------------------------");
                nnet.feedForward(trX);
                float trainCost = nnet.costFunc.cost((nnet.layerList.get(nnet.layerList.size() - 1).getOutputs()),(trY));
                float trainAcc = nnet.costFunc.accuracy((nnet.layerList.get(nnet.layerList.size() - 1).getOutputs()),(trY));
                nnet.feedForward(tsX);
                float testCost = nnet.costFunc.cost((nnet.layerList.get(nnet.layerList.size() - 1).getOutputs()),(tsY));
                float testAcc = nnet.costFunc.accuracy((nnet.layerList.get(nnet.layerList.size() - 1).getOutputs()),(tsY));
                System.out.printf("train data cost: %.5f acc: %.2f" , trainCost, trainAcc);
                System.out.println();
                System.out.printf("test  data cost: %.5f acc: %.2f" , testCost, testAcc);
                System.out.println();
                
                long estimatedBatchTime = System.currentTimeMillis() - startBatchTime;
                System.out.printf("------ batch time:%3d.%3d sec ------" , (int)(estimatedBatchTime/1000),estimatedBatchTime%1000);
                System.out.println();
            }
        }
        long estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println("estimatedTime: " + (int)(estimatedTime/1000)+"." + estimatedTime%1000 + " sec");

    }

}
