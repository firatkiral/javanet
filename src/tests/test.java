
package tests;

import java.util.stream.IntStream;


public class test
{
    static float getMax(float[] a)
    {
        float max = 0;
        for (float d : a)
        {
            if (d > max)
            {
                max = d;
            }
        }
        if (max == 0.0)
        {
            max = Float.MIN_VALUE;
        }
        return max;
    }

    static float getMin(float[] a)
    {
        float min = Float.MAX_VALUE;
        for (float d : a)
        {
            if (d < min)
            {
                min = d;
            }
        }
        return min;
    }
    
    public static float[][] normM(float[][] A)
    {
        float[][] temp = new float[A.length][A[0].length];

        IntStream.range(0, A.length)
                .parallel()
                .forEach(m ->
                {
                    float min = getMin(A[m]);
                    float max = getMax(A[m]) - min;
                    for (int n = 0; n < A[0].length; n++)
                    {
                        temp[m][n] = (A[m][n] - min) / max;
                    }
                });
        return temp;
    }
    
    public static void main(String[] args)
    {   
        float t = 0/6;
        int N = 2000;
        int cycle = 5;
        float [][] A = {
            {2,4,6,8},
            {1,3,5,7}
        };
        
        
    }
}
