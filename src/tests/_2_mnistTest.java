
package tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import neuralNet.old.activations;
import neuralNet.old.costFunctions;
import neuralNet.old.dataReader;
import neuralNet.old.nnet;
import static neuralNet.old.nMath.*;

public class _2_mnistTest
{
    static float [][] trX;
    static float [][] trY;
    static float [][] tsX;
    static float [][] tsY;
    
    static void initializeData(int ySplit, double testSplit)
    {
        int dataCount = dataReader.dataset.length;
//        dataReader.dataset = normN(dataReader.dataset, dataReader.dataset[0].length-1);
        int testCount = (int) (dataReader.dataset.length*testSplit);
        
        trX = new float[dataCount-testCount][];
        trY = new float[dataCount-testCount][];
        tsX = new float[testCount][];
        tsY = new float[testCount][];
        
        for (int i = 0; i < dataCount; i++)
        {
            if (trX.length > i)
            {
                trX[i] = Arrays.copyOfRange(dataReader.dataset[i], 0, ySplit);
                trY[i] = Arrays.copyOfRange(dataReader.dataset[i], ySplit, dataReader.dataset[i].length);
            }
            else
            {
                tsX[i-trX.length] = Arrays.copyOfRange(dataReader.dataset[i], 0, ySplit);
                tsY[i-trX.length] = Arrays.copyOfRange(dataReader.dataset[i], ySplit, dataReader.dataset[i].length);
            }
        }
        dataReader.dataset = new float[0][0];
    }
    
//    private static final Random rnd = new Random(System.currentTimeMillis());
    private static final Random rnd = new Random(5);
    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        long startTime = System.currentTimeMillis();
        dataReader.read("datasets/mnist.out", " ", rnd, true);
        long estimatedTime = System.currentTimeMillis() - startTime;
        System.out.printf("Read time:%3d.%3d sec" , (int)(estimatedTime/1000),estimatedTime%1000);
        System.out.println();
        
        startTime = System.currentTimeMillis();
        initializeData(784, .14286f);
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.printf("Initialize time:%3d.%3d sec" , (int)(estimatedTime/1000),estimatedTime%1000);
        System.out.println();
        
        startTime = System.currentTimeMillis();
        trY = encode(trY);
        tsY = encode(tsY);
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.printf("Encode time:%3d.%3d sec" , (int)(estimatedTime/1000),estimatedTime%1000);
        System.out.println();
        
        startTime = System.currentTimeMillis();
        int batchSize = 100;
        
        nnet nnet = new nnet(trX, trY, batchSize, rnd);
        nnet.addLayer(100, activations.RELU, .5f);
//        nnet.addLayer(40, activations.RELU, .15f);
        nnet.addLayer(10, activations.SOFTMAX);
//        nnet.addSGDtrainer(.1f, .3f, .01f, true);
        nnet.addADAMtrainer();
        nnet.build(costFunctions.categorical_crossentropy);
        
        estimatedTime = System.currentTimeMillis() - startTime;
        System.out.printf("Init NNET time:%3d.%3d sec" , (int)(estimatedTime/1000),estimatedTime%1000);
        System.out.println();
        System.out.println();

        long startRun = System.currentTimeMillis();
        for (int i = 0; i < 160; i++)
        {
            System.out.println("epoc: "+ i + " ----------------------------");
            
            startTime = System.currentTimeMillis();
            nnet.train();
            estimatedTime = System.currentTimeMillis() - startTime;
            System.out.printf("Training time:%3d.%3d sec" , (int)(estimatedTime/1000),estimatedTime%1000);
            System.out.println();
        
            if (i % 10 == 0)
            {
                startTime = System.currentTimeMillis();
                nnet.feedForward(trX);
                float trainCost = nnet.costFunc.cost((nnet.layerList.get(nnet.layerList.size() - 1).getOutputs()),(trY));
                float trainAcc = nnet.costFunc.accuracy((nnet.layerList.get(nnet.layerList.size() - 1).getOutputs()),(trY));
                estimatedTime = System.currentTimeMillis() - startTime;
                System.out.printf("FF TrainX time:%3d.%3d sec" , (int)(estimatedTime/1000),estimatedTime%1000);
                System.out.println();
                
                startTime = System.currentTimeMillis();
                nnet.feedForward(tsX);
                float testCost = nnet.costFunc.cost((nnet.layerList.get(nnet.layerList.size() - 1).getOutputs()),(tsY));
                float testAcc = nnet.costFunc.accuracy((nnet.layerList.get(nnet.layerList.size() - 1).getOutputs()),(tsY));
                estimatedTime = System.currentTimeMillis() - startTime;
                System.out.printf("FF TestX time:%3d.%3d sec" , (int)(estimatedTime/1000),estimatedTime%1000);
                System.out.println();
                
                System.out.printf("train data cost: %.5f acc: %.2f" , trainCost, trainAcc);
                System.out.println();
                System.out.printf("test  data cost: %.5f acc: %.2f" , testCost, testAcc);
                System.out.println();
                
                System.out.println("------------------------------------");
            }
        }
        estimatedTime = System.currentTimeMillis() - startRun;
        System.out.println("total time: " + (int)(estimatedTime/1000)+"." + estimatedTime%1000 + " sec");
    }

}
