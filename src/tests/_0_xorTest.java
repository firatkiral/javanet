
package tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;
import neuralNet.old.activations;
import neuralNet.old.costFunctions;
import neuralNet.old.nnet;

public class _0_xorTest
{
//    static private Random rnd = new Random(System.currentTimeMillis());
    private static final Random rnd = new Random(5);
    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        float[][] trainX = {{1,0},{0,1},{1,1},{0,0}};
        float[][] trainY = {{1},{1},{0},{0}};
        
        int batchSize = 1;
        int batchCount = 4;
        
        nnet nnet = new nnet(trainX, trainY, batchSize, rnd);
        nnet.addLayer(3, activations.SIGMOID);
        nnet.addLayer(1, activations.SIGMOID);
        nnet.addSGDtrainer(26f, .03f, .0001f, false);
//        nnet.addADAGRADtrainer();
//        nnet.addADAMtrainer();
        nnet.build(costFunctions.mse);

        long startTime = System.currentTimeMillis();
        
        for (int i = 0; i < 1000; i++)
        {
            nnet.train();
            if (i % 100 == 0)
            {
                System.out.println("---------------");
                System.out.println("epoc: "+ i);
                nnet.feedForward(trainX);
                float trainCost = nnet.costFunc.cost((nnet.layerList.get(nnet.layerList.size() - 1).getOutputs()),(trainY));
                float trainAcc = nnet.costFunc.accuracy((nnet.layerList.get(nnet.layerList.size() - 1).getOutputs()),(trainY));
                System.out.println("train data cost: " + trainCost + " acc: " + trainAcc);
                
                System.out.println("---------------");

            }
        }
        long estimatedTime = System.currentTimeMillis() - startTime;

        System.out.println("estimatedTime: " + (int)(estimatedTime/1000)+"." + estimatedTime%1000 + " sec");

    }

}
